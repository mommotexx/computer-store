//Declares user of application. Could be loaded from a database
const user = {
    "id": 1,
    "firstName": "Joe",
    "lastName": "Bankers",
    "balance": 5000,
    "currency": "NOK",
    "lang": "nb-NO",
    "hasLoan": false,
    "loanAmount": 0,
    "ownedComputers": []
};

// wage is the amount the user get every time they press work. 
let wage = 100;
let selectedComputer = 0;
let salary = 0;
let computers = [];
const localeCurrency = [user.lang, {style:"currency", currency:user.currency}];
const apiBase = "https://noroff-komputer-store-api.herokuapp.com";

//Declare consts for reusability
const btnLoan = document.getElementById("getLoan");
const btnBank = document.getElementById("bank");
const btnWork = document.getElementById("work");
const btnBuy = document.getElementById("buyComputer");
const selectLaptop = document.getElementById("selectLaptop");

const productName = document.getElementById("productName");
const productInfo = document.getElementById("productInfo");
const productImage = document.getElementById("productImage");
const productPrice = document.getElementById("price");
const productStock = document.getElementById("productStock");
const specList = document.getElementById("specList");

const userName = document.getElementById("userName");
const userBalance = document.getElementById("userBalance");
const userPay = document.getElementById("userPay");
const moneyInfo = document.getElementById("moneyInfo");
const userSection = document.getElementById("user");

//Runs when the browser has rendered the HTML content. 
window.onload = () => {
    fetchDisplayComputers();
    updateUserInformation();
}

//Functions
//Updates user information visible on the site
const updateUserInformation = () => {
    userName.innerHTML = user.firstName + " " + user.lastName;
    userBalance.innerHTML = user.balance.toLocaleString(localeCurrency[0], localeCurrency[1]);
    userPay.innerHTML = salary.toLocaleString(localeCurrency[0], localeCurrency[1]);
    if (document.getElementById("loanText") != null) {
        document.getElementById("loanText").innerHTML = `You have ${user.loanAmount.toLocaleString(localeCurrency[0], localeCurrency[1])} in loan`;
    }
}

//Gets data from API, stores the data in a variable, and calls updateDisplayComputers.
const fetchDisplayComputers = () => {
    fetch(apiBase + "/computers")
        .then(response => response.json())
        .then(data => {
            computers = data;
            updateDisplayComputers(computers);
        });
}

/* Create selection list, and updates view of computers. 
   If run for the first time(without selecting machine), it will get the first computer */
const updateDisplayComputers = (obj, index) => {
    if (index === undefined) {
        index = 0;

        obj.forEach(element => {
            let option = document.createElement("option");
            option.value = element.id;
            option.innerHTML = element.title;
            selectLaptop.appendChild(option);
        });

        computers[4].image = "assets/images/5.png"; //Hardfix for the bad image from the API.
    }
    productName.innerHTML = obj[index].title;
    productInfo.innerHTML = obj[index].description;
    productStock.innerHTML = "In stock: " + obj[index].stock;
    productPrice.innerHTML = obj[index].price.toLocaleString(localeCurrency[0], localeCurrency[1]);
    productImage.src = apiBase + "/" + obj[index].image;

    //Easy way to clear the spec list. 
    specList.replaceChildren();

    for (let spec of computers[index].specs) {
        const specInfo = document.createElement("li");
        specInfo.innerHTML = spec;
        specList.appendChild(specInfo);
    }
}

//Updates users loan. Checks if the user has loan, and if 0 or below, it will reset to 0, and remove button and text for loan
const updateLoan = () => {
    if (user.hasLoan) {
        if (user.loanAmount <= 0) {
            user.hasLoan = false;
            user.loanAmount = 0;
            addRemovePaymentLoan(false);
        }
    }
}

//Pays the reminding loan in full amount
const payLoanInFull = () => {
    if (user.balance >= user.loanAmount) {
        user.balance -= user.loanAmount;
        user.loanAmount = 0;
        updateLoan();
        updateUserInformation();
    }
}

const payFromPay = () => {
    if (salary <= user.loanAmount) {
        user.loanAmount -= salary;
        salary = 0;
        updateLoan();
        updateUserInformation();
    }
    if (salary > user.loanAmount) {
        const salaryAfterPaidLoan = (salary - user.loanAmount);
        user.loanAmount -= salary;
        user.balance += salaryAfterPaidLoan;
        salary = 0;
        updateLoan();
        updateUserInformation();
    }
}

//Adds button and text for loan if true is passed to the function. Removes if false is passed to the function. 
const addRemovePaymentLoan = (bool) => {
    if (bool) {
        const loanText = document.createElement("p");
        loanText.id = "loanText";
        moneyInfo.appendChild(loanText);

        const btnPayLoan = document.createElement("button");
        btnPayLoan.id = "btnPayLoan";
        btnPayLoan.innerHTML = "Pay loan in full";
        btnPayLoan.className = "btn yellow";
        userSection.appendChild(btnPayLoan);
        btnPayLoan.addEventListener("click", payLoanInFull);

        const btnPayFromPay = document.createElement("button");
        btnPayFromPay.id ="btnPayFromPay";
        btnPayFromPay.innerHTML = "Repay Loan";
        btnPayFromPay.className = "btn green";
        document.getElementsByClassName("work")[0].appendChild(btnPayFromPay);
        btnPayFromPay.addEventListener("click", payFromPay)
    }
    else {
        document.getElementById("loanText").remove();
        document.getElementById("btnPayLoan").remove();
        document.getElementById("btnPayFromPay").remove();
    }
}

//Adds wage to the salary, and updates the user info
const work = () => {
    salary += wage;
    updateUserInformation();
}

//Check if user input is reasonable. Then check if the user is eligible to take a loan, and then gives loan. 
const loanMoney = () => {
    let loanInput = prompt("Enter loan amount. Can not be more than 2x your current balance\nCan not apply for loan if you already have loan", 0);
    let loanInputNumber = +loanInput;
    if (!isNaN(loanInputNumber) && loanInputNumber <= (user.balance * 2) && loanInputNumber > 0) {
        if (!user.hasLoan) {
            user.balance += loanInputNumber;
            user.hasLoan = true;
            user.loanAmount = loanInputNumber;
            addRemovePaymentLoan(true);
            updateUserInformation();
        }
        else {
            alert("You already have " + user.loanAmount.toLocaleString(localeCurrency[0], localeCurrency[1])
            + " in loan. Please pay back before taking a new loan");
        }
    }
    else if (loanInputNumber > (user.balance * 2)) {
        alert("Your loan amount is too high.");
    }
    else {
        alert("Something went wrong");
    }
}

//Removes 10% of the salary if the user has loan, and adding to the bank. Else, it adds everything
const addSalaryToBank = () => {
    if (user.hasLoan) {
        const paidLoan = (salary / 10);
        const balanceAfterPaidLoan = salary - paidLoan;
        user.loanAmount -= paidLoan;
        user.balance += balanceAfterPaidLoan;
        updateLoan();
    }
    else {
        user.balance += salary;
    }
    salary = 0;
    updateUserInformation();
}

/* Buys a computer. Removes a computer from the stock, removes the price from the balance
   and pushes it to the users owned computers */
const buyComputer = () => {
    let thisComputer = computers[selectedComputer];
    if (thisComputer.stock > 0) {
        if (user.balance >= thisComputer.price) {
            user.balance -= thisComputer.price;
            thisComputer.stock--;
            alert("You bougt " + thisComputer.title + " for " + thisComputer.price.toLocaleString(localeCurrency[0], localeCurrency[1]));
            user.ownedComputers.push(thisComputer);
            updateUserInformation();
            updateDisplayComputers(computers, selectedComputer);
        }
        else {
            alert("You don't have enough money to buy this machine");
        }
    }
    else {
        alert("This machine is not in stock");
    }
}

//Handles laptopChange. Passes the selection to the function, to get the value property of the option selection
const laptopChange = () => {
    selectedComputer = selectLaptop.selectedIndex;
    updateDisplayComputers(computers, selectedComputer);
}

//Event-listeners
btnWork.addEventListener("click", work);
btnLoan.addEventListener("click", loanMoney);
btnBank.addEventListener("click", addSalaryToBank);
btnBuy.addEventListener("click", buyComputer);
selectLaptop.addEventListener("change", laptopChange);

/* Easter egg. Can you activate this using Konami code?
Credits to @Ehsan Kia for a simple event handler function I could use here and change. 
https://stackoverflow.com/questions/31626852/how-to-add-konami-code-in-a-website-based-on-html/48777893#48777893 */
let cursor = 0;
const konamiCode = ["ArrowUp", "ArrowUp", "ArrowDown", "ArrowDown", "ArrowLeft", "ArrowRight", "ArrowLeft", "ArrowRight", "KeyB", "KeyA"];
document.addEventListener("keydown", e => {
    cursor = e.code == konamiCode[cursor] ? cursor + 1 : 0;
    if (cursor == konamiCode.length) easterEgg();
});

const easterEgg = () => {
    let easterElement = document.createElement("div");
    easterElement.id = "easterElement";
    easterElement.style = `height: 100vh; width: 100vw; position: fixed; 
                           background-image: url("https://desktime.com/blog/wp-content/uploads/2017/07/mysl1jg.gif");
                           background-size: cover`;
    document.getElementsByTagName("body")[0].appendChild(easterElement);

    let btnClose = document.createElement("button");
    btnClose.id = "btnClose";
    btnClose.style = "width: 2rem; height: 2rem; float:right; border-radius: 50%; margin: 2rem;"
    btnClose.innerHTML = "X";
    document.getElementById("easterElement").appendChild(btnClose);
    btnClose.addEventListener("click", () => easterElement.remove());
}