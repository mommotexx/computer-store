<h1 align="center">Welcome to Computer Store 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1-blue.svg?cacheSeconds=2592000" />
</p>

> An online computer store, created as an assignment to learn javascript fundamentals, and to be able to interact with the DOM and API's

### ✨ [Live Demo](https://marcusjohansen.com/computer-store/)

## Usage

Download, and open the index.html page in a browser. Should work in most browsers except Internet Explorer

## Author

👤 **Marcus Vinje Johansen**

* Website: https://marcusjohansen.com
* Github: [@Mommotexx](https://github.com/Mommotexx)
* Gitlab: [@Mommotexx](https://gitlab.com/Mommotexx)
* LinkedIn: [@marcusvinje](https://linkedin.com/in/marcusvinje)

## Credits:
Thanks to [@Ehsan Kia](https://stackoverflow.com/users/451246/ehsan-kia) for making a compact and clean event listener for konami code, which I have edited to not be deprecated, and implemented an easter egg for my online store. 

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
